# Part 1: Software engineer

## 1.About Kuberneter*************
- Short introduction\
    Kubernetes is an open source platform for managing containerized workloads and services,that can help you to configuration your service to appropriate deploy
and process managing automation from your configuration.
    You can manage limit the resource of service and automate scaling,It allows you to manage Loadbalance easier.
    Furthermore,you can managing your service to automatically restart when the service is not working or down.
<br>
<br>


## 2. Layer 4 *************************
- 1.Use Network address translation(NAT)
- 2.Transmission Control Protocol(TCP) สำหรับการรับส่งข้อมูล Hypertext Transfer Protocol (HTTP) บนอินเทอร์เน็ต
- 3.Secure Sockets Layer(SSL) and TLS(Transport Layer Security)

<br>
<br>


## 3. Layer 7 *******************************
- 1.Domain Name Server(DNS)
- 2.Simple Mail Transfer Protocol(SMTP)
- 3.File Transfer Protocol(FTP)

<br>
<br>


## 4.Difference of I/O bound and CPU bound
- The difference.\
    CPU bound means that most processes are computed primarily by the CPU. It uses a small percentage of the I/O device or the rate at which the process is running is limited by CPU speed. Therefore,it make processing of CPU is slower than I/O processing times.

    I/O Bound means that most processes run primarily on the device and use a small percentage of the CPU process or the rate at which a process progresses is limited by the speed of the I/O or the rate at which a process progresses is limited by the speed of the I/O. Therefore,it make processing of I/O is slower than CPU processing times.
<br>
<br>

## 5.Encounters a CPU bound problem.
- Optimization.<br>

    1.Checking the CPU profile for why this problem occurs,to find a solution.
    2.If the written programming language is memory-managed by GC. Checking the percentage of garbage collection usage, to find a way to optimize.
    3.Finding a way to re-use memory.
    4.Using Multi-processiong.
    5.Using Load balancing > (Push migration and Pull migration)
    4.If the server used is a clound service,increase the number of cores or use faster CPUs.
<br>
<br>

## 6.encounters an I/O bound problem
- Optimization
    1.Optimizing memory usage
    2.Using Caching
    3.Use Multithreading so that the CPU distributes some of the completed job to I/O devices to run the next process immediately to balance the workload between the CPU and I/O.
<br>
<br>


## 7.

<br>
<br>


## 8.Pods on difference node communication.

<img src="./images/podtopod.jpg" alt="Alt text" title="Optional title">
    The illustration above explains the communication  between two Pods. The first Pod which running at IP 10.1.0.0 on NodeA and the second Pod which running at IP 10.1.1.1 on NodeB. Both of Pods communicated by Bridge(Router).
    - E.G: The pod IP(10.1.0.0) on NodeA need to communicate with the pod IP(10.1.1.1),It's necessary to API to Bridge first.To reached the addresses like 10.1.1.xxx,Then forward request to IP(10.1.1.1)
<br>
<br>

## 9.Kind of load balancer needed.
    It's Loadbalancer Ingress,Because it is an API object that provides routing rules to manage external users,Allow access to the services in a Kubernetes via HTTPS/HTTP. Besides,easily set up rules.


## 10.Demonstrate the authentication.
<img src="./images/JWT.jpg" alt="Alt text" title="Optional title"><br>
 - First Request,After user login to server. The server will validate user information from databases,If user data is verified. Then the server will generate JWT_token for response back to WebBrowser and then the WebBrowser will keep the JWT_token in LocalStorage. - The WebBrowser will beare the JWT_token for every request to send.
 - e.g. In step 4: If the user wishes to check information from the database on /me, the Browser will beare the token to the server for verification.After verification is successful, the user will get a basic information response from the server.